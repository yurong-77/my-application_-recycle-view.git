package com.example.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class Fragment_contacts extends Fragment {
    private Context context;
    //昵称列表
    private List<String> mList = new ArrayList<>();
    //个性签名列表
    private List<String> nList = new ArrayList<>();

    public Fragment_contacts() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        context = view.getContext();

        //添加数据
        InitNickNameList();
        InitSignatureList();

        //创建Adapter对象
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview_contacts);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(mList, nList, context);
        recyclerView.setAdapter(adapter);

        //LinearLayoutManager是一个工具类，承担了一个View(RecyclerView)的布局、测量、子View 创建、 复用、 回收、 缓存、 滚动等操作。
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
//        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));

        //addItemDecoration用来进行分割线设计
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));

        return view;
    }

    private void InitNickNameList() {
        mList.add("彩券");
        mList.add("认真的雪");
        mList.add("你还要我怎样");
        mList.add("绅士");
        mList.add("演员");
        mList.add("天外来物");
        mList.add("动物世界");
        mList.add("意外");
        mList.add("崇拜");
    }

    private void InitSignatureList() {
        nList.add("在我遇见你以前也拥有过完整的睡眠");
        nList.add("雪下的那么深，下的那么认真");
        nList.add("你停在了這條我們熟悉的街");
        nList.add("好久没见了什么角色呢");
        nList.add("簡單點 說話的方式簡單點");
        nList.add("你降落的 太突然了 我剛好呢 又路過了");
        nList.add("东打一下西戳一下 动物未必需要尖牙");
        nList.add("我在清晨的路上 誰被我遺忘");
        nList.add("抛弃世界吧 就剩我们俩");

    }


}
